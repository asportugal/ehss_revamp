

build image

    docker-compose build


run container nginx

    docker-compose up -d nginx


view logs

    docker-compose logs -f nginx laravel

run webpack

    npm run dev  // for building assets 
    npm run prod // for building minified assets