FROM php:7.1.10-fpm-alpine

# https://pkgs.alpinelinux.org/packages?name=php7*&branch=v3.6&repo=&arch=x86_64&maintainer=

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/main' > /etc/apk/repositories
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/community' >> /etc/apk/repositories
# RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/testing' >> /etc/apk/repositories
# RUN echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories

# shadow - usermod
RUN apk update \
  && apk upgrade \
  && apk add --no-cache nginx nodejs vim curl msmtp ca-certificates make less tzdata python su-exec libmcrypt-dev redis openssl imagemagick \
  mysql-client zip shadow

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# see all alpine package https://pkgs.alpinelinux.org/packages?name=php7-*&branch=edge&repo=&arch=x86_64&maintainer=
RUN apk add --no-cache libwebp libwebp-dev \
  && apk add --no-cache php7-fpm php7-mbstring php7-tokenizer php7-xml php7-redis php7-pdo_mysql php7-pdo \
  php7-zip php7-xsl php7-json php7-iconv php7-curl php7-gd php7-imagick php7-mailparse php7-gmp php7-exif

# RUN npm install --global yarn

RUN usermod -u 1000 www-data

COPY . /ehss

RUN chown -R www-data /ehss

USER www-data
WORKDIR /ehss/httpdocs
# RUN php artisan config:clear
# RUN composer dump-autoload
#RUN composer install
RUN composer update

USER root

ADD ./phpfpm/laravel.ini /usr/local/etc/php/conf.d
ADD ./phpfpm/laravel.pool.conf /usr/local/etc/php-fpm.d/

WORKDIR /ehss
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["nginx"]
