# http://codetunes.com/2012/06/20/nginxunicorn-configuration-for-multi-app-servers
# https://gist.github.com/1276230
# http://derekneely.com/2009/06/nginx-failed-13-permission-denied-while-reading-upstream/

server {
  root        /ehss/httpdocs/public;
  index       index.php index.html index.htm;
  listen      80;

  # development only
  # http://www.conroyp.com/2013/04/25/css-javascript-truncated-by-nginx-sendfile/
  sendfile off;

  keepalive_timeout     5;
  client_max_body_size  20M;

  access_log /dev/stdout main;
  error_log /dev/stdout debug;

  if ($request_method !~ ^(GET|HEAD|PUT|POST|DELETE|OPTIONS)$ ){
    return 405;
  }

  location / {
    try_files $uri $uri/ /index.php$is_args$args;
    error_page 404 /404.html;
    error_page 422 /422.html;
    error_page 500 502 503 504 /500.html;
    error_page 403 /403.html;
  }

  location ~ \.php$ {
    try_files $uri /index.php =404;
    fastcgi_pass laravel:9000;
    fastcgi_index index.php;
    fastcgi_buffers 16 16k;
    fastcgi_buffer_size 32k;

    fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
    fastcgi_param  QUERY_STRING       $query_string;
    fastcgi_param  REQUEST_METHOD     $request_method;
    fastcgi_param  CONTENT_TYPE       $content_type;
    fastcgi_param  CONTENT_LENGTH     $content_length;

    fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
    fastcgi_param  REQUEST_URI        $request_uri;
    fastcgi_param  DOCUMENT_URI       $document_uri;
    fastcgi_param  DOCUMENT_ROOT      $document_root;
    fastcgi_param  SERVER_PROTOCOL    $server_protocol;
    fastcgi_param  REQUEST_SCHEME     $scheme;
    fastcgi_param  HTTPS              $https if_not_empty;

    fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
    fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

    fastcgi_param  REMOTE_ADDR        $remote_addr;
    fastcgi_param  REMOTE_PORT        $remote_port;
    fastcgi_param  SERVER_ADDR        $server_addr;
    fastcgi_param  SERVER_PORT        $server_port;
    fastcgi_param  SERVER_NAME        $server_name;

    # PHP only, required if PHP was built with --enable-force-cgi-redirect
    fastcgi_param  REDIRECT_STATUS    200;
  }

  location = /favicon.ico {
    expires     max;
    add_header  Cache-Control public;
  }

}

