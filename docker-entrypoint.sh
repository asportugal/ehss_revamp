#!/bin/sh
set -e

if [ "$1" = 'sh' ]; then
  exec $@
  exit
fi

if [ "$1" = 'nginx' ]; then
  exec nginx -c /ehss/nginx/nginx.conf -g 'daemon off;'
  exit
fi

cd httpdocs
if [ ! -f '.env' ]; then
  echo 'copying .env.example to .env'
  cp .env.example .env
  cat .env
fi
# [ ! -f '.env' ] && cp .env.example .env
# su-exec www-data composer update
su-exec www-data composer install

# output the container environment for debugging
echo 'current environment settings'
env | sort

# php artisan migrate:install
if [ "$1" = 'migrate' ]; then
  su-exec www-data php artisan migrate
  exit
fi

# not needed anymore stored in APP_KEY environment
# php artisan key:generate

# seed data
php artisan migrate
#php artisan db:seed
#php artisan elastic:sync

if [ "$1" = 'php-fpm' ]; then
  php-fpm -F
  exit
fi

if [ "$1" = 'php-queue' ]; then
  php artisan queue:work --queue=high,default
  exit
fi

exec $@
