webpackJsonp([4],{

/***/ "./resources/assets/js/app.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_filters__ = __webpack_require__("./resources/assets/js/components/filters.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_mixins__ = __webpack_require__("./resources/assets/js/components/mixins.js");




toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

var moduleName = document.querySelector('meta[name="page"]').content;

__webpack_require__.p = window.location.origin;

__webpack_require__("./resources/assets/js/src lazy recursive ^\\.\\/.*$")("./" + moduleName).then(function (module) {
    return new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(module.default);
}).catch(function (error) {
    return alert('Module not found');
});

/***/ }),

/***/ "./resources/assets/js/components/filters.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);


__WEBPACK_IMPORTED_MODULE_0_vue___default.a.filter('titleCase', function (val) {
    if (!val) return '';
    var newValue = '';
    var wordArray = val.split('-');
    for (var x in wordArray) {
        newValue += wordArray[x].charAt(0).toUpperCase() + wordArray[x].slice(1) + ' ';
    }

    return newValue;
});

__WEBPACK_IMPORTED_MODULE_0_vue___default.a.filter('removeString', function (val, strToRemove) {
    if (!val) return '';
    return val.toString().replace(strToRemove, '');
});

/***/ }),

/***/ "./resources/assets/js/components/mixins.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);


__WEBPACK_IMPORTED_MODULE_0_vue___default.a.mixin({
    mounted: function mounted() {
        var hideOnInit = this.$options.hideOnInit;
        if (hideOnInit) {
            this.$el.style.visibility = 'visible';
        }
    }
});

/***/ }),

/***/ "./resources/assets/js/src lazy recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./form-builder": [
		"./resources/assets/js/src/form-builder.js",
		0
	],
	"./form-builder.js": [
		"./resources/assets/js/src/form-builder.js",
		0
	],
	"./form-list": [
		"./resources/assets/js/src/form-list.js",
		2
	],
	"./form-list.js": [
		"./resources/assets/js/src/form-list.js",
		2
	],
	"./form-preview": [
		"./resources/assets/js/src/form-preview.js",
		1
	],
	"./form-preview.js": [
		"./resources/assets/js/src/form-preview.js",
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./resources/assets/js/src lazy recursive ^\\.\\/.*$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./resources/assets/sass/app.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./resources/assets/js/app.js");
module.exports = __webpack_require__("./resources/assets/sass/app.scss");


/***/ })

},[0]);