<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Form;
use App\Validation\ValidateSchemaBuilder;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'forms' => Form::all()
        ];

        return view('forms.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => []
        ];

        return view('forms.form-builder', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validator::make($request->all(), [
        //     'schema' => new ValidateSchemaBuilder()
        // ])->validate();

        Form::create([
            'schema' => json_decode($request->schema)
        ]);

        return response()->json([
            'message' => 'Form succesfully created!'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $Form
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = [
            'form' => Form::find($id)
        ];

        return view('forms.preview', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [
            'form' => Form::find($id)
        ];

        return view('forms.form-builder', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = Form::find($id);
        $form->schema = json_decode($request->schema);
        $form->save();

        return response()->json([
            'message' => 'Form succesfully updated!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Form::find($id)->delete();
        return response()->json([
            'message' => 'Form succesfully deleted'
        ]);
    }


    public function accordion(){
        return view('layout.section-nav');
    }
}
