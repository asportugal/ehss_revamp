<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table    = 'forms';
    protected $fillable = ['schema'];
    protected $casts    = [
        'schema' => 'array'
    ];

    /**
     * one to one relationships with document
     */
    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }
}
