<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

    // belongs to a form
    public function form()
    {
      return $this->belongsTo('App\Models\FormBuilder', 'form_id');
    }

    public function user()
    {
      return $this->belongsTo('App\Models\User', 'user_id');
    }
}
