<?php

namespace App\Validation;

use App\Models\FormBuilder;
use Illuminate\Contracts\Validation\Rule;

class ValidateSchemaBuilder implements Rule
{
    protected $message = [];

    public function passes($attribute, $value)
    {
        $data = json_decode($value);
        $response = true;

        if (FormBuilder::where('schema->title', $data->title)) {
            $this->message[] = '{"title": "Title " . $data->title . " already exists"}';
            $response = false; 
        }

        if (FormBuilder::where('schema->name', $data->name)) {
            $this->message[] = '{"name": "Name " . $data->name . " already exists"}';
            $response = false;
        }
        
        return $response;

    }

    public function message()
    {
        return json_encode($this->message);
    }
}
