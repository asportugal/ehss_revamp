import Vue from 'vue';

Vue.mixin({
    mounted() {
        let hideOnInit = this.$options.hideOnInit;
        if (hideOnInit) {
            this.$el.style.visibility = 'visible';
        }
    }
})