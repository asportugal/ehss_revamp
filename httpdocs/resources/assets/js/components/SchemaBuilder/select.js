import inflection from 'inflection';

export default {
    name: 'select-schema',
    props: ['selected_schema'],
    template: `<div class="modal-content">
                    <div class="modal-header">
                        <slot name="title"></slot>
                    </div>
                    <form @submit.prevent="saveFieldSchema()">
                    <div class="modal_body">
                    <div class="clearfix">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="nav-item"><a class="nav-link" :class="{ 'active': active_tab == 1 }" @click="active_tab = 1">Display</a></li>
                            <li role="presentation" class="nav-item"><a class="nav-link" :class="{ 'active': active_tab == 2 }" @click="active_tab = 2">Attributes</a></li>
                            <li role="presentation" class="nav-item"><a class="nav-link" :class="{ 'active': active_tab == 3 }" @click="active_tab = 3">Options</a></li>
                        </ul>
                        <div id="tab-content" class="col-md-12">
                            <div v-if="active_tab == 1">
                                <div class="m-form__group form-group">
                                    <label>Label</label>
                                    <input type="text" name="label" v-model="schema.label" class="form-control m-input m-input--square" required/>
                                </div>
                                <div class="m-form__group form-group">
                                    <label>Size</label>
                                    <input type="number" name="size" v-model="schema.size" class="form-control m-input m-input--square" required/>
                                </div>
                                <div class="m-form__group form-group">
                                    <div class="m-checkbox-list">
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="required" v-model="schema.has_blank" :value="true"> Has Blank?
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div v-if="active_tab == 2">
                                <div class="m-form__group form-group">
                                    <label>Name</label>
                                    <input pattern="^[a-z_]+$" type="text" class="form-control m-input m-input--square" v-model="schema.name" name="name" title="Only accepts lowercase letters and underscore. e.g. field_name"" required/>
                                </div>
                                <div class="m-form__group form-group">
                                    <label>Class</label>
                                    <textarea class="form-control m-input m-input--square" v-model="schema.class" name="class"></textarea>
                                </div>
                                <div class="m-form__group form-group">
                                    <div class="m-checkbox-list">
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="required" v-model="schema.required" :value="true"> Required?
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div v-if="active_tab == 3">
                                <table>
                                    <thead>
                                        <td><strong>Items</strong></td>
                                        <td></td>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(option, index) in schema.options">
                                            <td><input type="text" class="form-control m-input m-input--square" v-model="schema.options[index]" name="option[]"></td>
                                            <td>
                                                <button type="button" class="btn m-btn--square btn-primary m-btn m-btn--icon m-btn--icon-only" v-if="index == 0" @click="addOption()"><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn m-btn--square btn-secondary m-btn m-btn--icon m-btn--icon-only" v-else><i class="fa fa-minus" @click="removeOption(index)"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>   
                    </div>
                </div>
            <div class="modal-footer">
                <div class="button-group col-md-12 text-right">
                    <button type="submit" class="btn m-btn--square btn-primary btn-md">Save</button>
                    <button type="button" class="btn m-btn--square btn-danger btn-md" @click="closeModal()">Cancel</button>
                </div>  
            </div>
            </form>
        </div>`,
    data() {
        return {
            active_tab: 1,
            schema: {
                id: '',
                type: 'select-field',
                label: '',
                size: '',
                required: false,
                name: '',
                class: '',
                has_blank: false,
                options: ['']
            }
        }
    },
    watch: {
        'schema.label'(val) {
            this.schema.label = inflection.titleize(val);
            this.schema.name = inflection.underscore(val).replace(/\s/g, '');
        },
        'schema.class'(val) {
            this.schema.class = inflection.dasherize(val);
        }
    },
    methods: {
        saveFieldSchema() {
            if (this.schema.label == '') {
                this.active_tab = 1;
            } else if (this.schema.name == '') {
                this.active_tab = 2;
            } else {
                this.closeModal();
                this.$emit('save', this.schema);
            }
        },
        addOption() {
            this.schema.options.push('')
        },
        removeOption(index) {
            this.schema.options.splice(index, 1)
        },
        closeModal() {
            this.$parent.show_field_form = false;
        }
    },
    mounted() {
        if (Object.keys(this.selected_schema).length === 0) {
            var now = Date.now();
            this.schema.id = now.toString();
        } else {
            this.schema = this.selected_schema;
        }
    }
}