import inflection from 'inflection';

export default {
    name: 'section-builder',
    props: ['selected_schema'],
    template: `<div class="modal-content">
                    <div class="modal-header">
                        <slot name="title"></slot>
                    </div>
                    <form @submit.prevent="saveFieldSchema()">
                    <div class="modal_body">
                    <div class="clearfix">
                        <div id="tab-content" class="col-md-12" style="min-height: 280px">
                            <div> 
                                <table class="col-md-12">
                                    <thead>
                                        <td><strong>items</strong></td>
                                        <td></td>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(items, index) in schema.items">
                                            <td><input type="text" class="form-control m-input m-input--square" v-model="schema.items[index]['title']" @keyup="populateName($event, index)"></td>
                                            <td>
                                                <button type="button" class="btn m-btn--square btn-primary m-btn m-btn--icon m-btn--icon-only" v-if="index == 0" @click="addItem()"><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn m-btn--square btn-danger m-btn m-btn--icon m-btn--icon-only" v-else><i class="fa fa-minus" @click="removeItem(index)"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> 
                        </div>  
                    </div>
                </div>
            <div class="modal-footer">
                <div class="button-group col-md-12 text-right">
                    <button type="submit" class="btn m-btn--square btn-primary btn-md">Save</button>
                    <button type="button" class="btn m-btn--square btn-metal btn-md" @click="closeModal()">Cancel</button>
                </div>  
            </div>
            </form>
        </div>`,
    data() {
        return {
            active_tab: 1,
            schema: {
                id: '',
                type: 'section-layout',
                class: '',
                items: [{
                    name: '',
                    title: '',
                    content: null
                }]
            }
        }
    },
    methods: {
        populateName(e, index) {
            let val = e.target.value;
            this.schema.items[index].title = inflection.titleize(val);
            this.schema.items[index].name = inflection.underscore(val).replace(/\s/g, '');
        },
        saveFieldSchema() {
            this.closeModal();
            this.$emit('save', this.schema);
        },
        closeModal() {
            this.$parent.show_field_form = false;
        },
        addItem() {
            this.schema.items.push({
                name: '',
                title: '',
                content: null
            })
        },
        removeItem(index) {
            this.schema.items.splice(index, 1)
        }
    },
    mounted() {
        if (Object.keys(this.selected_schema).length === 0) {
            var now = Date.now();
            this.schema.id = now.toString();
        } else {
            this.schema = this.selected_schema;
        }
    }
}