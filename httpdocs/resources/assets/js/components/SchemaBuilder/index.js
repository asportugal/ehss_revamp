const flattenObject = (obj) => {
    let newObj = {};
    Object.getOwnPropertyNames(obj).map(name => {
        Object.assign(newObj, obj[name]);
    });

    return newObj;
}

export let schemaList = {
    basic: {
        'text-input-builder': require('./text-input.js').default,
        'checkbox-builder': require('./checkbox.js').default,
        'radio-builder': require('./radio.js').default,
        'long-text-builder': require('./long-text.js').default,
        'select-builder': require('./select.js').default,
        'date-time-builder': require('./date-time.js').default,
        'date-range-builder': require('./date-range.js').default
    },
    layout: {
        'section-builder': require('./section.js').default,
        'tab-builder': require('./tab.js').default,
        'table-builder': require('./table.js').default
    }
}

export let builderComponents = flattenObject(schema)