import Vue from 'vue';

Vue.filter('titleCase', function (val) {
    if (!val) return '';
    let newValue = '';
    let wordArray = val.split('-');
    for (let x in wordArray) {
        newValue += wordArray[x].charAt(0).toUpperCase() + wordArray[x].slice(1) + ' ';
    }

    return newValue;
});

Vue.filter('removeString', function(val, strToRemove) {
    if (!val) return '';
    return val.toString().replace(strToRemove, '');
});