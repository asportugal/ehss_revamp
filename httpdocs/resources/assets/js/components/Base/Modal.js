export default {
    name: 'modal',
    props: {
        toggle: Boolean,
        width: {
            type: Number,
            default: 50,
            validator: function (value) {
                return value <= 100
            }       
        }
    },
    watch: {
        toggle(val) {
            if(val) {
                $('#modal').modal('show');
            } else {
                $('#modal').modal('hide');
            }
        }
    },
    template: `<div class="modal fade" tabindex="-1" role="dialog" id="modal" aria-hidden="true">
                    <div class="modal-dialog" role="document">
					    <div class="modal-content">
						    <div class="modal-header">
							    <h5 class="modal-title"><slot name="title"></slot></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <slot name="body"></slot>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>`
}