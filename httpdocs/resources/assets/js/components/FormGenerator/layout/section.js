export default {
    name: 'section-layout',
    props: ['schema'],
    template: `
        <div :id="schema.id">
            <section v-for="(item, index) in schema.items">
                <div class="section-heading">
                    {{ item.title }}
                </div>
                <div class="section-body">
                </div>
            </section>
        </div>
    ` 
}