export default {
    'section-layout': require('./section.js').default,
    'tab-layout': require('./tab.js').default,
    'table-layout': require('./table.js').default
}
