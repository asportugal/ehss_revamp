export default {
    name: 'long-text',
    props: ['schema'],
    template: `<div class="form-group" :id="schema.id">
        <slot name="operation-btn"></slot>
        <label v-text="schema.label"></label>
        <textarea class="form-control m-input m-input--square" @input="inputValue($event)" :class="schema.class" :rows="schema.rows" :name="schema.name" :required="schema.required"></textarea>
    </div>
    `,
    methods: {
        inputValue(e) {
            this.$emit('input', {value: e.target.value, from: this.schema.name});
        }
    }
}