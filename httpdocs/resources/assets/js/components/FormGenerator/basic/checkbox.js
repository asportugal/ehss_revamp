export default {
    name: 'checkbox',
    props: ['schema'],
    template: `<div class="form-group" :id="schema.id">
    <slot name="operation-btn"></slot>
    <label v-text="schema.label"></label>
    <template v-if="schema.inline">
        <div class="m-checkbox-inline">
            <label v-for="(item, index) in schema.items" class="m-checkbox">
                <input type="checkbox" @click="inputValue($event)" :class="schema.class" :name="schema.name" :value="item"> {{ item }}
                <span></span>
            </label>
        </div>
    </template>
    <template v-else>
        <div class="m-checkbox-list">
            <label v-for="(item, index) in schema.items" class="m-checkbox">
                <input type="checkbox" @click="inputValue($event)" :class="schema.class" :name="schema.name" :value="item"> {{ item }}
                <span></span>
            </label>
        </div>
    </template>
    </div>
    `,
    methods: {
        inputValue(e) {
            console.log(e);
            this.$emit('input', { value: e.target.value, from: this.schema.name });
        }
    }
}