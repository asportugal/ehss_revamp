export default {
    'text-input-field': require('./text-input.js').default,
    'checkbox-field': require('./checkbox.js').default,
    'radio-field': require('./radio.js').default,
    'long-text-field': require('./long-text.js').default,
    'select-field': require('./select.js').default,
    'date-time-field': require('./date-time.js').default,
    'date-range-field': require('./date-range.js').default
}
