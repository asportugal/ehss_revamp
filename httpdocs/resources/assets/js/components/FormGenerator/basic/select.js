export default {
    name: 'select',
    props: ['schema'],
    template: `<div class="form-group" :id="schema.id">
        <slot name="operation-btn"></slot>
        <label v-text="schema.label"></label>
        <select class="form-control m-input m-input--square" :class="schema.class" @change="inputValue($event)" :name="schema.name">
            <option v-if="schema.has_blank"></option>
            <option v-for="(option, index) in schema.options" :value="option">{{ option }}</option>
        </select>
    </div>
    `,
    methods: {
        inputValue(e) {
            this.$emit('input', { value: e.target.value, from: this.schema.name });
        }
    }
}