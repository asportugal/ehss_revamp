export default {
    name: 'text-input-field',
    props: ['schema'],
    template: `<div class="form-group" :id="schema.id">
        <slot name="operation-btn"></slot>
        <label v-text="schema.label"></label>
        <input type="text" @input="inputValue($event)" class="form-control m-input m-input--square" :class="schema.class" :placeholder="schema.placeholder" :size="schema.size" :name="schema.name" :required="schema.required">
    </div>
    `,
    methods: {
        inputValue(e) {
            this.$emit('input', { value: e.target.value, from: this.schema.name });
        }
    }
}