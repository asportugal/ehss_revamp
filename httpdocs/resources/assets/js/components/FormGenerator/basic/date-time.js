import moment from 'moment';

export default {
    name: 'date',
    props: ['schema'],
    template: `<div class="form-group" :id="schema.id">
    <slot name="operation-btn"></slot>
        <label v-text="schema.label"></label>
        <div class="input-group m-input-group--square" style="z-index: 0;">
            <input type="text" id="m_datepicker" :name="schema.name" :class="schema.class" style="background: #fff" class="form-control m-input--square" readonly="" :required="schema.required">
            <div class="input-group-append">
                <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                </span>
            </div>
        </div>
    </div>
   `,
    mounted() {
        let vm = this;
        $('#m_datepicker').datetimepicker({
            format: this.schema.format,
            todayHighlight: true,
            todayBtn: 'linked',
            clearBtn: true,
            autoclose: true,
            templates: {
                leftArrow: '<i class="fa fa-angle-left"></i>',
                rightArrow: '<i class="fa fa-angle-right"></i>'
            }
        }).on('change', function(e) {
            let date = moment(e.target.value, vm.schema.format).creationData();
            let value = moment(date.input).format('YYYY-MM-DD');
            vm.$emit('input', { value: value, from: e.target.name });
        });
   }
}
