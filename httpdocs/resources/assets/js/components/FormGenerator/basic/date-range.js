export default {
    name: 'date-range',
    props: ['schema'],
    template: `<div class="form-group" :id="schema.id">
    <slot name="operation-btn"></slot>
        <label v-text="schema.label"></label>
        <div class="input-daterange input-group m-input-group--square" id="m_daterangepicker" style="z-index: 0;">
            <input type="text" :name="schema.name_start" style="background: #fff" class="form-control m-input--square" readonly="" :required="schema.required">
            <div class="input-group-append">
				<span class="input-group-text">
					<i class="la la-ellipsis-h"></i>
				</span>
			</div>
            <input type="text" :name="schema.name_end" style="background: #fff" class="form-control m-input--square" readonly="" :required="schema.required">
            <div class="input-group-append">
				<span class="input-group-text">
					<i class="fa fa-calendar"></i>
				</span>
			</div>
        </div>
    </div>
   `,
    mounted() {
        let vm = this;
        $('#m_daterangepicker').datepicker({
            format: this.schema.format,
            todayHighlight: true,
            autoclose: true,
            todayBtn: 'linked',
            clearBtn: true,
            templates: {
                leftArrow: '<i class="fa fa-angle-left"></i>',
                rightArrow: '<i class="fa fa-angle-right"></i>',
            }
        }).on('change', function (e) {
            let date = moment(e.target.value, vm.schema.format).creationData();
            let value = moment(date.input).format('YYYY-MM-DD');
            vm.$emit('input', { value: value, from: e.target.name });
        });
    }
}
