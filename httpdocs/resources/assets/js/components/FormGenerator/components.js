import basic from './basic';
import layout from './layout';

export default Object.assign({}, basic, layout);