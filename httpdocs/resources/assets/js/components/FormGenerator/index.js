import BasicComponents from './basic';
import LayoutComponents from './layout';

export default {
    name: 'form-generator',
    components: { BasicComponents, LayoutComponents },
    props: {
        schema: {
            type: Object
        }
    },
    template: `<form :name="schema.name">
        <slot name="title"></slot>
        <component v-for="(component, index) in schema.layout" @input="setFieldValue($event)" :key="index" :is="component.type" :schema="component">
        </component>
    </form>`,
    methods: {
        setFieldValue(e) {
            this.$emit('input', e);
        }
    }
}