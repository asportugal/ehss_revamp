import axios from 'axios';

export default {
    el: '#app',
    hideOnInit: true,
    data: {
        token: '',
        recordSet: []
    },
    methods: {
        deleteForm(index) {
            let response = confirm('Are you sure you want to delete?')

            if(response) {
                let id = this.recordSet[index].id;
                
                axios.post('/form/' + id, { '_method': 'DELETE', token: this.token })
                .then(response => { 
                    toastr.success(response.data.message, 'Success'); 
                    this.recordSet.splice(index, 1);
                })
                .catch(error => { toastr.error(error, 'Error'); });
            }


        }
    },
    created() {
        let table = document.getElementById('form-table');
        this.recordSet = JSON.parse(table.dataset.recordset);
        table.dataset.recordset = '';

        this.token = document.querySelector('meta[name="token"]').content;
    }
}