import { schemaList, builderComponents } from '../components/SchemaBuilder';
import FormGenerator from '../components/FormGenerator/components';
import Modal from '../components/Base/Modal';
import draggable from 'vuedraggable';
import inflection from 'inflection';
import axios from 'axios';

export default {
    el: '#app',
    hideOnInit: true,
    components: {
        Modal,
        FormGenerator,
        builderComponents,
        draggable,
    },
    data: {
        modal_toggle: false,
        current_index: 0,
        selected_component: {},
        operation: '',
        dragging: false,
        schema: {
            name: '',
            title: '',
            layout: []
        }
    },
    watch: {
        'schema.title'(val) {
            this.schema.title = inflection.titleize(val);
            this.schema.name = inflection.underscore(val).replace(' ', '');
        }
    },
    methods: {
        startDrag(e) {
            console.log('start e', e);
        },
        saveForm(e) {
            let data = { schema: JSON.stringify(this.schema), token: this.token };
            
            if(e.target.dataset.id != '') {
                data['_method'] = 'PUT'; 
            }

            axios.post(e.target.action, data)
                .then(response => { toastr.success(response.data.message, 'Success'); })
                .catch(error => { toastr.error(error, 'Error'); });
        },
        editComponent(index, type) {
            this.operation = 'edit';
            this.selected_field = type.replace('-field', '-builder');
            this.current_index = index;
            this.show_field_form = true;
        },
        removeComponent(index) {
            this.schema.layout.splice(index, 1);
        },
        addComponent(e) {


            // if(this.operation == 'add') {
            //     this.schema.layout.splice(this.current_index, 0, e);
            // }

            // if(this.operation == 'edit') {
            //     this.schema.layout[this.current_index] = e;
            // }
        },
        deleteForm(id) {
            axios.post('/form' + id, {
                'method': 'DELETE',
                '_token': this.token
            }).then(function(response) {
                console.log(response);
            });
        },
        checkMove(e, originalEvent) {
            console.log('e', e);
            console.log('orig', originalEvent);
        }
    },
    computed: {
        group_components() {
            return schemaList;
        },
        current_selected_component() {
            if(this.operation == 'add') return {};

            if (typeof this.schema.layout.indexOf(this.current_index) !== -1) {
                return this.schema.layout[this.current_index];
            }

            return {};
        }
    },
    created() {
        this.token = document.querySelector('meta[name="token"]').content;
        let schema = document.getElementById('schema');
        let value = JSON.parse(schema.value);
        if (value) {
            this.schema = value;
        }
        schema.parentNode.removeChild(schema);
    },  
    mounted() {
        // let previewEl = document.getElementById('form-preview');

        // let update = (evt) => {
        //     let realList = this.schema.layout.slice(0);
        //     let origSchema = realList.splice(evt.oldIndex, 1);
        //     realList.splice(evt.newIndex, 0, origSchema[0]);
            
        //     this.$set(this.schema, 'layout', realList);
        // }
        
        // let sortable = new Sortable(previewEl, { sort: true, onUpdate: update });
    }

}