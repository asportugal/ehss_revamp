import FormGenerator from '../components/FormGenerator';

export default {
    el: '#app',
    hideOnInit: true,
    components: { 'form-generator' : FormGenerator },
    data: {
        model: {}
    },
    methods: {
        getFieldValue(e) {
            this.$set(this.model, e.from, e.value);
        }
    }
}