import Vue from 'vue';
import filters from './components/filters';
import mixins from './components/mixins';

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

let moduleName = document.querySelector('meta[name="page"]').content;

__webpack_public_path__ = window.location.origin;

import('./src/' + moduleName)
    .then(module => new Vue(module.default))
    .catch(error => alert('Module not found'));
