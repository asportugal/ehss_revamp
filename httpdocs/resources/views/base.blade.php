<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="page" content="@yield('page')">
    <meta name="token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EHS Revamp</title>
    <link rel="stylesheet" type="text/css" href="/metronic/base/vendors.bundle.css">
    <link rel="stylesheet" type="text/css" href="/css/metronic.bundle.css">
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <div class="m-grid m-grid--hor m-grid--root m-page">                                                 <!-- begin:: Page -->
        @include('layout.header')
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body"> <!-- begin::Body -->
            @include('layout.aside')
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                @include('layout.crumbs')
                <div class="m-content" style="position: relative">
                    <div class="m--font-primary" style="position: absolute; right: 50%; top: 30%">
                        <i class="fa fa-spinner fa-spin " style="font-size: 40px"></i>
                    </div>
                    <div id="app" style="visibility: hidden">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>       
        @include('layout.footer')
    </div>                                                                                               <!-- end:: Page -->
    <script src="/js/jquery.min.js"></script>
    <script src="/metronic/base/vendors.bundle.js"></script>
    <script src="/metronic/metronic.bundle.js"></script>

    <script src="{{ mix('/js/manifest.js') }}"></script>
    <script src="{{ mix('/js/vendor.js') }}"></script>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
