@extends('base')
@section('page', 'form-preview')
@section('content')
<div class="row">
    <div class="col-xl-12 with-section-navigation">
        <div class="m-portlet m-portlet--mobile">
            <div id="app">
                <div class="m-portlet__head">
                   <div class="m-portlet__head-caption">
                       <div class="m-portlet__head-title">
                           <h3 class="m-portlet__head-text">
                               {{ $form->schema['title'] }}
                           </h3>
                       </div>
                   </div>
                </div>
                <div class="m-portlet__body">
                   <form-generator id="formGenerator" :schema="{{ json_encode(optional($form)->schema) }}" @input="getFieldValue($event)">
                   </form-generator>
                </div>
            </div>
        </div>
    </div>
    @include('layout.section-nav')
</div>
@endsection
