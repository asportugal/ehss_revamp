@extends('base')
@section('page', 'form-builder')
@section('content')

<div class="row">
    <div class="col-xl-12 ">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            FORM BUILDER
                        </h3>
                    </div>
                </div>
            </div>
            <form class="m-form" @submit.prevent="saveForm($eventent)" action="/form/{{ optional($form)->id }}" id="schema-builder" data-id="{{ optional($form)->id }}" autocomplete="off" novalidate>
                <input type="hidden" id="schema" value="{{ json_encode(optional($form)->schema) }}">
                <div class="row m-portlet__body">
                    <div class="col-md-12">
                        <div class="form-group m-form__group">
                            <label>Title: </label>
                            <input type="text" class="form-control m-input m-input--square" name="title" v-model="schema.title" required>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Name: </label>
                            <input type="text" class="form-control m-input m-input--square" name="name" v-model="schema.name" pattern="^[a-z_]+$" title="Only accepts lowercase letters and underscore. e.g. field_name" required>
                        </div>
                    </div>
                    <div class="col-md-2 form-group m-form__group">
                        <div v-for="(group, group_name) in group_components" 
                            class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">@{{ group_name | titleCase }} Component</h3>
                                    </div>      
                                </div>
                            </div>
                            <draggable v-model="Object.keys(group)" 
                                :options="{group:{ name:'components',  pull:'clone', put:false }}"
                                :move="checkMove"
                                @start="startDrag($event)">
                                <div v-for="(component, component_name, index) in group" class="list-group-item" :key="index">
                                    @{{ component_name | removeString('builder') | titleCase }}
                                </div>
                            </draggable>
                        </div>
                    </div>
                    <div class="col-md-10 form-group m-form__group">
                        <div class="m-portlet m-portlet--success m-portlet--head-solid-bg m-portlet--bordered">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">Preview</h3>
                                    </div>      
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <draggable v-model="schema.layout" style="min-height: 50px" :options="{group:'components'}" @add="addComponent($event)">
                                    <li v-for="(component, index) in schema.layout">
                                        @{{ component }}
                                    </li>
                                    <!-- <component :key="index" :schema="component" :is="component.type" @save="addField($event)">
                                    </component> -->
                                </draggable>
                            </div>
                            <!-- END FIELD PROPERTIES MODAL -->
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <button type="submit" class="btn m-btn--square btn-primary">Save</button>
                        <a href="/" class="btn m-btn--square btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL -->
<modal :toggle="modal_toggle">
    <p slot="title">@{{ selected_component }}</p>
    <template v-if="selected_component.length">
        <component :is="selected_component" 
            :selected_schema="current_selected_component"
            slot="body"
            @save="addComponent($event)" 
            @cancel="removeComponent($event)">
        </component>
    </template>
</modal>
<!-- MODAL -->
@endsection
