<div class="section-navigation">
    <!--Begin::Section Navigation -->
    <div class="section-navigation__items">
        <div class="section-navigation__item">
            <div class="section-navigation__item-cricle">
                <a href="#">
            		<i class="fa fa-circle m--font-danger" data-toggle="m-popover" title="" data-content="IS ACTIVE"></i>
            	</a>
            </div>
        </div>

        <div class="section-navigation__item">
            <div class="section-navigation__item-cricle">
                <a href="#">
            		<i class="fa fa-circle-o m--font-success" data-toggle="m-popover" title="" data-content="NOT ACTIVE"></i>
            	</a>
            </div>
        </div>

        <div class="section-navigation__item">
            <div class="section-navigation__item-cricle">
                <a href="#">
            		<i class="fa fa-circle-o m--font-primary" data-toggle="m-popover" title="" data-content="SECTION C"></i>
            	</a>
            </div>
        </div>

        <div class="section-navigation__item">
            <div class="section-navigation__item-cricle">
                <a href="#">
            		<i class="fa fa-circle-o m--font-warning" data-toggle="m-popover" title="" data-content="SECTION D"></i>
            	</a>
            </div>
        </div>

        <div class="section-navigation__item">
            <div class="section-navigation__item-cricle">
                <a href="#">
            		<i class="fa fa-circle-o m--font-info" data-toggle="m-popover" title="" data-content="SECTION D"></i>
            	</a>
            </div>
        </div>
    </div>

    <!--End::Section Nagivation 2 -->
</div>
